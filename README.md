# [Ruhacker Lab](http://www.ruhacker.space) - [Grayscale](http://startbootstrap.com/template-overviews/grayscale/)
This is the website repo for ruhacker.space the official website for RuhackerLab, your friendly neighbourhood hack space, hacking for civic good in Ruaka.

## Getting Started

To get started:
* Download the latest release on Start Bootstrap
* Fork this repository on GitHub

## Bugs and Issues

Have a bug or an issue with the website? [Open a new issue](https://bitbucket.org/nj3ma/ruhacker.space/issues) here on BitBucket or  [template overview page at Start Bootstrap](http://startbootstrap.com/template-overviews/grayscale/).

## Creator
This repo is a fork of Start Bootstrap which was created by and is maintained by **David Miller**, Managing Parter at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2016 Ruhacker Lab. Code released under the [Apache 2.0](https://github.com/IronSummitMedia/startbootstrap-grayscale/blob/gh-pages/LICENSE) license.